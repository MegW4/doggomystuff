﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace doggo3
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        async void Handle_WalksClicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new dogWalker());
        }

        async void Handle_SittersClicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new dogSitter());
        }

        async void Handle_MeetClicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new dogMeet());
        }

        async void Handle_GroomingClicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new dogGroom());
        }
    }
}
