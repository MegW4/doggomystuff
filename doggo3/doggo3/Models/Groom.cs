﻿using System;
using System.Collections.Generic;
using System.Text;

namespace doggo3.Models
{
    public class Groom
    {
        public string ImageText { get; set; }
        public string IconSource { get; set; }
        public string ShortDescription { get; set; }
        public string Url { get; set; }

        public List<Groom> GetCellItem()
        {
            List<Groom> cells = new List<Groom>()
            {
                new Groom()
                {
                    ImageText = "PetSmart", ShortDescription = "Have qualified dog groomers.",
                    IconSource = "https://images.pexels.com/photos/356378/pexels-photo-356378.jpeg?auto=compress&cs=tinysrgb&h=350",
                    Url = "https://www.petsmart.com/pet-services/grooming/?gclid=CjwKCAjwoKDXBRAAEiwA4xnqv140RfbdHG3IgSYqKv76bSTzbAm0LNdeQ_4LnR918b39Qvkc_FNKUxoCjrYQAvD_BwE"
                },

                new Groom()
                {
                    ImageText = "Fritzy Mobile Grooming", ShortDescription = "Services: Spa Treatment, Remoisturizing Treatment, Deshedding Treatment, Teeth Brushing",
                    IconSource = "https://images.pexels.com/photos/58997/pexels-photo-58997.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
                    Url = "https://fritzyspetcarepros.com/Index.aspx"
                },

            };
            return cells;
        }
    }
}
