﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using doggo3.Models;

namespace doggo3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MoreInfo2 : ContentPage
	{
		public MoreInfo2 ()
		{
			InitializeComponent ();
		}
        public MoreInfo2(Meet meets)
        {
            InitializeComponent();
            BindingContext = meets;
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var button = (Button)sender;
            Meet meets = (Meet)button.CommandParameter;
            var uri = new Uri(meets.Url);
            Device.OpenUri(uri);
        }
    }
}