﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using doggo3.Models;

namespace doggo3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MoreInfo3 : ContentPage
	{
		public MoreInfo3 ()
		{
			InitializeComponent ();
		}
        public MoreInfo3(Sitter sit)
        {
            InitializeComponent();
            BindingContext = sit;
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var button = (Button)sender;
            Sitter sit = (Sitter)button.CommandParameter;
            var uri = new Uri(sit.Url);
            Device.OpenUri(uri);
        }
    }
}