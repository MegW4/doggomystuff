﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using doggo3.Models;

namespace doggo3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MoreInfo4 : ContentPage
	{
		public MoreInfo4 ()
		{
			InitializeComponent ();
		}

        public MoreInfo4(Walker walks)
        {
            InitializeComponent();
            BindingContext = walks;
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var button = (Button)sender;
            Walker walks = (Walker)button.CommandParameter;
            var uri = new Uri(walks.Url);
            Device.OpenUri(uri);
        }
    }
}