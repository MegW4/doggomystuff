﻿using System;
using System.Collections.Generic;
using System.Text;
using doggo3.Models;

namespace doggo3.ViewModel
{
    public class ViewGroom
    {
        public List<Groom> Cells { get; set; }

        public ViewGroom()
        {
            Cells = new Groom().GetCellItem();
        }
    }
}
