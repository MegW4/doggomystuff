﻿using System;
using System.Collections.Generic;
using System.Text;
using doggo3.Models;

namespace doggo3.ViewModel
{
    public class ViewMeet
    {
        public List<Meet> Cells { get; set; }

        public ViewMeet()
        {
            Cells = new Meet().GetCellItem();
        }
    }
}
