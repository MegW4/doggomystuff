﻿using System;
using System.Collections.Generic;
using System.Text;
using doggo3.Models;

namespace doggo3.ViewModel
{
    public class ViewSitter
    {
        public List<Sitter> Cells { get; set; }

        public ViewSitter()
        {
            Cells = new Sitter().GetCellItem();
        }
    }
}
