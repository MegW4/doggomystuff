﻿using System;
using System.Collections.Generic;
using System.Text;
using doggo3.Models;

namespace doggo3.ViewModel
{
    public class ViewWalker
    {
        public List<Walker> Cells { get; set; }

        public ViewWalker()
        {
            Cells = new Walker().GetCellItem();
        }
    }
}
