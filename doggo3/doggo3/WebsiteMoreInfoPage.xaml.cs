﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using doggo3.Models;

namespace doggo3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class WebsiteMoreInfoPage : ContentPage
	{
		public WebsiteMoreInfoPage ()
		{
			InitializeComponent ();
		}

        public WebsiteMoreInfoPage(Groom groomer)
        {
            InitializeComponent();
            BindingContext = groomer;
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var button = (Button)sender;
            Groom groomer = (Groom)button.CommandParameter;
            var uri = new Uri(groomer.Url);
            Device.OpenUri(uri);
        }
	}
}