﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;
using doggo3.Models;
using doggo3.ViewModel;

namespace doggo3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class dogGroom : ContentPage
    {
        ViewGroom vm;
        public dogGroom()
        {
            InitializeComponent();
            vm = new ViewGroom();
            dogGroomCells.ItemsSource = vm.Cells;
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            // Do whatever refresh logic you want here

            // Remember you have to set IsRefreshing False
            dogGroomCells.IsRefreshing = false;

        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            Groom itemTapped = (Groom)listView.SelectedItem;
            var uri = new Uri(itemTapped.Url);
            Device.OpenUri(uri);
        }

        void Handle_ContextMenuMoreButton(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var groomer = (Groom)menuItem.CommandParameter;
            Navigation.PushAsync(new WebsiteMoreInfoPage(groomer));
        }
    }
}
