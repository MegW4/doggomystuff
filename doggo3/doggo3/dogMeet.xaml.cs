﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using doggo3.Models;
using doggo3.ViewModel;

namespace doggo3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class dogMeet : ContentPage
    {
        ViewMeet vm;
        public dogMeet()
        {
            InitializeComponent();
            vm = new ViewMeet();
            dogMeetCells.ItemsSource = vm.Cells;
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            // Do whatever refresh logic you want here

            // Remember you have to set IsRefreshing False
            dogMeetCells.IsRefreshing = false;

        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            Meet itemTapped = (Meet)listView.SelectedItem;
            var uri = new Uri(itemTapped.Url);
            Device.OpenUri(uri);
        }

        void Handle_ContextMenuMoreButton(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var meetup = (Meet)menuItem.CommandParameter;
            Navigation.PushAsync(new MoreInfo2(meetup));
        }
    }
}
