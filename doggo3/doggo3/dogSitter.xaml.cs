﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using doggo3.Models;
using doggo3.ViewModel;

namespace doggo3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class dogSitter : ContentPage
    {
        ViewSitter vm;
        public dogSitter()
        {
            InitializeComponent();
            vm = new ViewSitter();
            dogSitterCells.ItemsSource = vm.Cells;
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            // Do whatever refresh logic you want here

            // Remember you have to set IsRefreshing False
            dogSitterCells.IsRefreshing = false;

        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            Sitter itemTapped = (Sitter)listView.SelectedItem;
            var uri = new Uri(itemTapped.Url);
            Device.OpenUri(uri);
        }

        void Handle_ContextMenuMoreButton(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var sits = (Sitter)menuItem.CommandParameter;
            Navigation.PushAsync(new MoreInfo3(sits));
        }
    }
}
